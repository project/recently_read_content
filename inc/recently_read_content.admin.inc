<?php

/**
 * @file
 * Recently read content admin functions.
 */

/**
 * Admin settings form.
 */
function recently_read_content_settings($form, &$form_state) {
  // Page-based restrictions.
  $form['recently_read_content']['recently_read_content_view_mode'] = array(
    '#type' => 'textfield',
    '#title' => t('View mode'),
    '#default_value' => variable_get('recently_read_content_view_mode', 'teaser'),
  );
  $form['recently_read_content']['recently_read_content_max_length'] = array(
    '#type' => 'textfield',
    '#title' => t('Max length'),
    '#default_value' => variable_get('recently_read_content_max_length', RECENTLY_READ_CONTENT_MAX_LENGTH),
    '#element_validate' => array('element_validate_integer_positive'),
  );
  $form['recently_read_content']['recently_read_content_default_queue'] = array(
    '#type' => 'textfield',
    '#title' => t('Show default queue once empty'),
    '#description' => t('Once no recently read content, the default queue content will show'),
    '#default_value' => variable_get('recently_read_content_default_queue', ''),
  );
  $form['recently_read_content']['pages'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => t('Pages'),
  );
  $form['recently_read_content']['pages']['recently_read_content_restrictions_page'] = array(
    '#type' => 'radios',
    '#options' => array(
      0 => t('All pages except those listed'),
      1 => t('Only the listed pages'),
    ),
    '#title' => t('Log requests on specific pages'),
    '#default_value' => variable_get('recently_read_content_restrictions_page', 0),
  );
  $form['recently_read_content']['pages']['recently_read_content_restrictions_pages'] = array(
    '#type' => 'textarea',
    '#rows' => 2,
    '#default_value' => variable_get('recently_read_content_restrictions_pages', ''),
    '#description' => t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array(
      '%blog' => 'blog',
      '%blog-wildcard' => 'blog/*',
      '%front' => '<front>',
    )),
  );
  return system_settings_form($form);
}
