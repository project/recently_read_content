<?php

/**
 * @file
 * Recently read content common functions.
 */

/**
 * AJAX callback to return recently read content list.
 */
function jjbos_apac_storefinder_ajax_load() {
  return TRUE;
}

function recently_read_content_ajax_callback() {
  // Get recently read content nid list.
  $nids = $_POST['recentlyReadContent'];
  $nodes = array();
  if (!empty($nids)) {
    $nodes = node_load_multiple($nids);
  }
  $output = '';
  $view_mode = variable_get('recently_read_content_view_mode', 'teaser');
  foreach ($nodes as $node) {
    $output .= '<div class="row">';
    $output .= drupal_render(node_view($node, $view_mode));
    $output .= '</div>';
  }
  print $output;
}
