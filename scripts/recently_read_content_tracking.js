(function ($) {
  Drupal.behaviors.recentlyReadContentTracking = {
    attach: function (content, settings) {
      var checkHistory = function(nid) {
        var recentlyReadContent = JSON.parse(localStorage.getItem('recentlyReadContent'));
        if (recentlyReadContent == '' || recentlyReadContent == null) {
          // Initial the array if there is no recently read contents.
          recentlyReadContent = [];
        }
        var index = recentlyReadContent.indexOf(nid);
        // Check if the current view content already in recently read contents list.
        if (index >= 0) {
          // Remove existing nid.
          recentlyReadContent.splice(index, 1);
        }
        // Push current view content at the head of array.
        recentlyReadContent.unshift(nid);
        // Keep the the max allowed size.
        recentlyReadContent = recentlyReadContent.slice(0, 6);
        // Set back to local storage.
        localStorage.setItem('recentlyReadContent', JSON.stringify(recentlyReadContent));
      };
      // Attach load event.
      if (settings.recently_read_content.currentNid) {
        var nid = settings.recently_read_content.currentNid;
        $(window).on('load', checkHistory(nid));
      }
    }
  }
})(jQuery);
