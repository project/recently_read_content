(function ($) {
  Drupal.behaviors.recentlyReadContentLoading = {
    attach: function (content, settings) {

      var loadRecentlyReadContent = function() {
        $('#recently-read-content-list').load('/ajax/recently-read-content/load', {
          'recentlyReadContent': JSON.parse(localStorage.getItem('recentlyReadContent'))
        });
      }

      $(window).on('load', loadRecentlyReadContent());
    }
  }
})(jQuery);

