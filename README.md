The Recently Read Content module displays the history of recently read content
a particular user has viewed which stored under browser localStorage.

== INSTALLATION ==

* Install as usual, see http://drupal.org/node/70151 for further information.
* Module depend on module entityqueue.
